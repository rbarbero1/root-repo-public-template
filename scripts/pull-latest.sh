#!/bin/bash

### TODO: This is VERY linear and needs refactoring (or a diffrerent approach all together, perhaps/likely kustomize)

# Required
# go install github.com/mikefarah/yq/v4@latest

echo "Extract and convert Helm charts to ACM format"

DEST_DIR=$1 # directory

if [[ -z "${DEST_DIR}" ]]; then
    echo "ERROR: Please specify a directory to use as a base (ie: 'external-secrets' or 'longhorn')"
    exit 1
fi

if [[ -d "output" ]]; then
    rm -rf $DEST_DIR/output
fi

mkdir -p $DEST_DIR/output

if [[ -d "generated" ]]; then
    rm -rf $DEST_DIR/generated
fi

if [[ "${DEST_DIR}" == "external-secrets" ]]; then
    echo "Converting External Secrets"
    ## Add and update the helm repo
    helm repo add external-secrets https://charts.external-secrets.io
    helm repo update external-secrets

    ## Generate YAML output
    helm template external-secrets \
        external-secrets/external-secrets \
        -n external-secrets > $DEST_DIR/output/external-secrets-full.yaml

    # Split into "namespace" and "non-namespace"
    kubernetes-split-yaml --outdir $DEST_DIR/generated --template_sel tpl_ns $DEST_DIR/output/external-secrets-full.yaml

fi

if [[ "${DEST_DIR}" == "longhorn" ]]; then
    echo "Converting External Secrets"
    ## Pull latest release
    # curl -o ${DEST_DIR}/output/longhorn.yaml https://raw.githubusercontent.com/longhorn/longhorn/v1.3.0/deploy/longhorn.yaml

    # cat ${DEST_DIR}/output/longhorn.yaml

    # # Split into "namespace" and "non-namespace"
    # kubernetes-split-yaml --outdir $DEST_DIR/generated --template_sel tpl_ns $DEST_DIR/output/longhorn.yaml


    # echo "Removing namespace file (already exists)"
    # rm -rf ${DEST_DIR}/generated/_no_ns_/longhorn-system.Namespace.yaml

    # echo "Removing default configmap (recommended)"
    # rm -rf ${DEST_DIR}/generated/longhorn-system/longhorn-default-setting.ConfigMap.yaml

    helm repo add longhorn https://charts.longhorn.io
    helm repo update longhorn

    ## Generate YAML output

    helm template longhorn \
        longhorn/longhorn \
        -n longhorn-system > $DEST_DIR/output/longhorn-full.yaml

    # Split into "namespace" and "non-namespace"
    kubernetes-split-yaml --outdir $DEST_DIR/generated --template_sel tpl_ns $DEST_DIR/output/longhorn-full.yaml

    # echo "Removing namespace file (already exists)"
    rm -rf ${DEST_DIR}/generated/_no_ns_/longhorn-system.Namespace.yaml

    # echo "Removing default configmap (recommended)"
    rm -rf ${DEST_DIR}/generated/longhorn-system/longhorn-default-setting.ConfigMap.yaml

    # echo "Removing Job for Uninstall"
    rm -rf ${DEST_DIR}/generated/longhorn-system/longhorn-uninstall.Job.yaml

    # Remove ".status" from all of the CRDs
    CLUSTER_FILES=`ls ${DEST_DIR}/generated/_no_ns_/*CustomResourceDefinition.yaml`
    for FILE in $CLUSTER_FILES
    do
        yq -i 'del(."status")' ${FILE}
    done


fi

echo "Files can be copied to root-repo folders now..."
